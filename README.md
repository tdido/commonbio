# commonbio

An interactive visualiser of genome-level similarities and differences between organisms.

Originally developed for the [SRUK Art & Science Inititative](https://artandscience2017.com/)

Also presented at [Fundación Telefónica during the Science Week in Madrid](https://espacio.fundaciontelefonica.com/evento/artscience-llega-a-madrid/)
