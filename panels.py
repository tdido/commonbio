import os
import random
import pygame
from pygame.mixer import Sound, get_init, pre_init
import math
import sys
import json
import numpy as np

from luma.core.interface.serial import i2c, spi
from luma.core.render import canvas
from luma.oled.device import ssd1306
from PIL import Image,ImageFont,ImageDraw

import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)

import matplotlib
from matplotlib import cm

def getconfig():
    with open(os.path.dirname(os.path.realpath(__file__)) + "/config.json") as f:
        return json.load(f)

config = getconfig()
ROOTDIR = config["ROOTDIR"] 
config["interval"] = config["intervalinit"]
config["soundfreqmax"] = config["soundfreqmaxinit"]
config["cmap"] = matplotlib.cm.get_cmap(config["colourmaps"][0])
config["select"] = None
config["content"] = config["oled"]["control"]["content"][:]

UPDATE_PANELS_OLED = True

class Note(Sound):
    def __init__(self, frequency, volume=.3):
        self.frequency = frequency
        Sound.__init__(self, self.build_samples())
        self.set_volume(volume)

    def build_samples(self):
        period = int(round(get_init()[0] / self.frequency))
        samples = np.array([0] * period)
        amplitude = 2 ** (abs(get_init()[1]) - 1) - 1
        for time in range(period):
            if time < period / 2:
                samples[time] = amplitude
            else:
                samples[time] = -amplitude
        return samples

def play(freqs):
    for f in freqs:
        n = Note(f).play(-1)

def make_font(name, size):
    font_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'fonts', name))
    return ImageFont.truetype(font_path, size)

def get_icon(oid):
    return("{}/img/icons/{}.png".format(ROOTDIR,oid))

if config["windowed"]:
    screen = pygame.display.set_mode((640,480))
else:
    screen = pygame.display.set_mode((0,0),pygame.FULLSCREEN | pygame.NOFRAME)
pygame.display.set_caption("CommonBio")
screenw, screenh = pygame.display.get_surface().get_size()

orgs = config["orgs"]

for o in orgs:
    o["data"] = {"cur":[],"seq":[]}
    with open("{}/data/{}.csv".format(ROOTDIR,o["id"])) as ifh:
        for r in ifh:
            seq,curr = r.rstrip("\n").split(",")
            o["data"]["cur"].append(float(curr))
            o["data"]["seq"].append(seq)

    o["icon"] = Image.open(get_icon(o["id"])).convert("RGBA")
    if config["showicons"]:    
        arr_img = plt.imread(get_icon(o["id"]), format='png')
        imagebox = OffsetImage(arr_img, zoom=1)
        imagebox.image.axes = ax
        o["iconbox"] = imagebox

##BEGIN OLED PANELS

oled = config["oled"]

def update_panels_oled(p,o):
    oled["panels"]["background"].paste(o["icon"], (oled["panels"]["x"][p],20))

font_title = make_font("DejaVuSans.ttf", 12)
font_content = make_font("DejaVuSans.ttf", 11)

if oled["panels"]["enabled"]:
    oled["panels"]["serial"] = i2c(port=1, address=0x3C)
    oled["panels"]["device"] = ssd1306(oled["panels"]["serial"]) 
    oled["panels"]["background"] =  Image.new("RGBA", oled["panels"]["device"].size, "black")
    draw_panels = ImageDraw.Draw(oled["panels"]["background"])
    draw_panels.text((0, 0),oled["panels"]["title"],(255,255,0),font=font_title)
    oled["panels"]["device"].display(oled["panels"]["background"].convert(oled["panels"]["device"].mode))

def update_select_oled(selected_idx=None):
    try:
        content = config["content"]

        #loop the content, add ">" if selected, replace ">" with " " if not selected.
        if selected_idx != None:
            for i,c in enumerate(content):
                if selected_idx == i: 
                    content[i] = ">" + content[i][1:]
                elif ">" in c:
                    content[i] = content[i].replace(">"," ")

        oled["control"]["background"] =  Image.new("RGBA", oled["control"]["device"].size, "black")
        draw_control = ImageDraw.Draw(oled["control"]["background"])
        draw_control.text((0, 0),oled["control"]["title"],(255,255,0),font=font_title)
        draw_control.text((0, 20),"{}: {}".format(content[0],config["cmap"].name),(255,255,0),font=font_content)
        draw_control.text((0, 35),"{}: {}".format(content[1],config["interval"]),(255,255,0),font=font_content)
        draw_control.text((0, 50),"{}: {}Hz".format(content[2],config["soundfreqmax"]),(255,255,0),font=font_content)
        oled["control"]["device"].display(oled["control"]["background"].convert(oled["control"]["device"].mode))
    except TypeError:
        pass

if oled["control"]["enabled"]:
    oled["control"]["serial"] = i2c(port=1, address=0x3D)
    oled["control"]["device"] = ssd1306(oled["control"]["serial"]) 
    update_select_oled()

##END OLED PANELS

##BEGIN PANELS

panels = config["panels"]
npanels = len(panels)
ngaps = npanels - 1

containerw = config["containerpctw"] * screenw 
containerh = config["containerpcth"] * screenh
containerx = (screenw - containerw) / 2 + config["pcxcorr"]
containery = (screenh - containerh) / 2 + config["pcycorr"]

panelswfull = containerw / npanels
panelsgap = panelswfull * config["panelsgappct"]
panelswgapped = panelswfull - (ngaps * panelsgap) / npanels
panelsh = containerh

#calculate panel coordinates
for i,p in enumerate(sorted(panels)):
    x = containerx + panelswgapped * i
    x = x + panelsgap * i if i > 0 else x
    panels[p]["coords"] = (x,containery)

def update_panel(p,di=0):
    global panels
    panels[p]["orgi"] += di

    i = panels[p]["orgi"]
    if i < 0:
        panels[p]["orgi"] = len(orgs) - 1
    elif i > len(orgs) - 1:
        panels[p]["orgi"] = 0

    if config["showicons"]:
        o = orgs[panels[p]["orgi"]]
        try:
            panels[p]["icon"].remove()
        except AttributeError:
            pass
        ab = AnnotationBbox(o["iconbox"], (0,0), xybox=(panels[p]["coords"][0]+0.065,panels[p]["coords"][1] + (pheight/2) + 0.034))
        panels[p]["icon"] = ax.add_artist(ab)

def panelevent(channel):
    global panels
    global UPDATE_PANELS_OLED
    for p in panels:
        if panels[p]["clk"] == channel or panels[p]["dt"] == channel:
            panel = panels[p]
            panel["clkstate"] = GPIO.input(panel["clk"])
            panel["dtstate"] = GPIO.input(panel["dt"])
            if panel["clkstate"] == 0 and panel["dtstate"] == 1:
                update_panel(p,-1)
                UPDATE_PANELS_OLED = True
            elif panel["clkstate"] == 1 and panel["dtstate"] == 0:
                update_panel(p,1)
                UPDATE_PANELS_OLED = True

##END PANELS

def resetevent(pin):
    global soundfreqs
    config["interval"] = config["intervalinit"]
    config["cmap"] = matplotlib.cm.get_cmap(config["colourmaps"][0])
    config["soundfreqmax"] = config["soundfreqmaxinit"]
    soundfreqs = (config["soundfreqmaxinit"] - config["soundfreqmin"])  
    update_select_oled()

def ctrlevent(channel):
    def change(mult):
        if config["select"] == "top":
            ncm = len(config["colourmaps"])
            newindex = config["colourmaps"].index(config["cmap"].name) + mult
            if newindex > ncm - 1:
                newindex = 0
            elif newindex < 0:
                newindex = ncm - 1
            config["cmap"] = matplotlib.cm.get_cmap(config["colourmaps"][newindex])
        elif config["select"] == "center":
            newint = config["interval"] + config["intervalstep"] * mult * -1
            if newint < config["intervalrange"][0]:
                newint = config["intervalrange"][0]
            elif newint > config["intervalrange"][1]:
                newint = config["intervalrange"][1]
            config["interval"] = newint

        elif config["select"] == "bottom":
            config["soundfreqmax"] -= config["soundfreqstep"] * mult
            if config["soundfreqmax"] > config["soundfreqmaxrange"][1]:
                config["soundfreqmax"] = config["soundfreqmaxrange"][1]
            elif config["soundfreqmax"] < config["soundfreqmaxrange"][0]:
                config["soundfreqmax"]  = config["soundfreqmaxrange"][0]

        update_select_oled()

    global soundfreqs
    clkstate = GPIO.input(config["gpio"]["ctrl"]["clk"])
    dtstate = GPIO.input(config["gpio"]["ctrl"]["dt"])
    if clkstate == 0 and dtstate == 1:
        change(-1)
    elif clkstate == 0 and dtstate == 0:
        change(1)

    soundfreqs = (config["soundfreqmax"] - config["soundfreqmin"])  

GPIO.setup(config["gpio"]["reset"], GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.add_event_detect(config["gpio"]["reset"], GPIO.RISING, callback=resetevent, bouncetime=1000)

GPIO.setup(config["gpio"]["select"]["top"], GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(config["gpio"]["select"]["bottom"], GPIO.IN, pull_up_down=GPIO.PUD_UP)

GPIO.setup(config["gpio"]["ctrl"]["clk"], GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(config["gpio"]["ctrl"]["dt"], GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.add_event_detect(config["gpio"]["ctrl"]["clk"], GPIO.BOTH, callback=ctrlevent)
GPIO.add_event_detect(config["gpio"]["ctrl"]["dt"], GPIO.BOTH, callback=ctrlevent, bouncetime=1000)

nanocurrmin=10000
nanocurrmax=0

for p in panels:
    GPIO.setup(panels[p]["clk"], GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(panels[p]["dt"], GPIO.IN, pull_up_down=GPIO.PUD_UP)

    panels[p]["clkstate"] = 0
    panels[p]["dtstate"] = 0

    GPIO.add_event_detect(panels[p]["clk"], GPIO.BOTH, callback=panelevent,bouncetime=100)
    GPIO.add_event_detect(panels[p]["dt"], GPIO.BOTH, callback=panelevent,bouncetime=100)

    update_panel(p)
    nanocurrmin = min(nanocurrmin,min(orgs[panels[p]["orgi"]]["data"]["cur"]))
    nanocurrmax = max(nanocurrmax,max(orgs[panels[p]["orgi"]]["data"]["cur"]))

nanocurrs = (nanocurrmax - nanocurrmin)  
soundfreqs = (config["soundfreqmaxinit"] - config["soundfreqmin"])  

colournorm = matplotlib.colors.Normalize(vmin=nanocurrmin, vmax=nanocurrmax)

config["cmap"] = matplotlib.cm.get_cmap(config["colourmaps"][0])

def transfreq(nanocurr):
    return round((((nanocurr - nanocurrmin) * soundfreqs) / nanocurrs) + config["soundfreqmin"],2)

#os.environ["SDL_VIDEO_CENTERED"] = "1"

clock = pygame.time.Clock()

class Panel(object):
    def __init__(self,x,y,h,w):
        self.rect = pygame.rect.Rect((x, y, w, h))
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.font = pygame.font.SysFont("dejavusans", config["fontsize"])


    #def handle_keys(self):
    #    key = pygame.key.get_pressed()
    #    dist = 1
    #    if key[pygame.K_LEFT]:
    #       self.rect.move_ip(-1, 0)
    #    if key[pygame.K_RIGHT]:
    #       self.rect.move_ip(1, 0)
    #    if key[pygame.K_UP]:
    #       self.rect.move_ip(0, -1)
    #    if key[pygame.K_DOWN]:
    #       self.rect.move_ip(0, 1)

    def draw(self, surface, colour, orgi, curr, seq):
        pygame.draw.rect(screen, colour, self.rect)

        colourL = []
        for c in colour:
            c = c / 255.0
            if c <= 0.03928:
                c = c/12.92
            else:
                c = ((c+0.055)/1.055) ** 2.4
            colourL.append(c)
                
        r,g,b = colourL
        L = 0.2126 * r + 0.7152 * g + 0.0722 * b
        font_colour = (0,0,0) if L > 0.179 else (255,255,255)

        tw,th = self.font.size("") #get height of text to calculate positioning
        tx = self.x + config["textoffsetx"]
        ty = self.y + self.h/2 - th * 2.5 #TODO: better way of centering text (auto calculate this 2.5) 

        outline_width = 0.5
        for i,t in enumerate([orgs[orgi]["id"],"({})".format(orgs[orgi]["name"]), "", "Secuencia ADN: " + seq, "Frecuencia sonido: {:.2f}".format(curr)]):
            ptext = self.font.render(t, True, font_colour) 
            screen.blit(ptext, (tx, ty + th * i))

pygame.mixer.pre_init(frequency=44000, size=-16, channels=1, buffer=1024)
pygame.init()

for p in panels:
    panels[p]["object"] = Panel(panels[p]["coords"][0],panels[p]["coords"][1],panelsh,panelswgapped)

clock = pygame.time.Clock()

def getc(curr):
    c = [int(n * 255) for n in config["cmap"](colournorm(curr))[0:3]]
    return c

running = True       
i = 0

while running:
    try:
        pygame.mixer.stop()
        freqs = [transfreq(orgs[panels[p]["orgi"]]["data"]["cur"][i]) for p in panels]
        play(freqs)
    except IndexError:
        i = 0
        continue

    for event in pygame.event.get():
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
            running = False
            break

    if not GPIO.input(config["gpio"]["select"]["top"]):
        if config["select"] != "top":
            config["select"] = "top"
            update_select_oled(0)
    elif not GPIO.input(config["gpio"]["select"]["bottom"]):
        if config["select"] != "bottom":
            config["select"] = "bottom"
            update_select_oled(2)
    else:
        if config["select"] != "center":
            config["select"] = "center"
            update_select_oled(1)

    if oled["panels"]["enabled"] and UPDATE_PANELS_OLED:
        [update_panels_oled(p,orgs[panels[p]["orgi"]]) for p in panels]
        oled["panels"]["device"].display(oled["panels"]["background"].convert(oled["panels"]["device"].mode))
        UPDATE_PANELS_OLED = False

    #screen.fill((0, 0, 0))
    for p in panels:
        curr = orgs[panels[p]["orgi"]]["data"]["cur"][i]
        seq = orgs[panels[p]["orgi"]]["data"]["seq"][i]
        panels[p]["object"].draw(screen,getc(curr),panels[p]["orgi"],curr,seq)

    pygame.display.update()

    clock.tick(config["interval"])

    i += 1
